
//Stores http request method
var getRequest;
var APIdata$;
var APIdata; /// local storage
var ResquestResponse;
var Contacts 


//##################### Enabling routing ##################////
var app = angular.module("InterviewApp", ['ngRoute']);
app.config(function ($routeProvider) {
    $routeProvider
        .when("/", {
            templateUrl: "home.html"
        })
        .when("/view", {
            templateUrl: "view.html"
        })
        .when("/home", {
            templateUrl: "home.html"
        })
});



//##################### Make Htpp request for new contacts data ##################////
function DealsWithHttpRequests($http) {
    return function () {
        return $http.get('http://localhost:8000/api/contacts');
    };
}
angular
    .module('InterviewApp')
    .factory('DealsWithHttpRequests', DealsWithHttpRequests);   