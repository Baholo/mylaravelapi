<?php

namespace App\Http\Requests\Requests;

use Illuminate\Foundation\Http\FormRequest;

class StoreContactRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'first_name' => 'required|max:255|unique:contacts',
            'last_name' => 'required|max:255|unique:contacts',
            'email' => 'required|unique:contacts',
            'mobile_number' => 'required|min:10|max:10|unique:contacts',
        ];
    }
}
