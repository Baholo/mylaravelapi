<?php

namespace App\Http\Resources\Contact;

use Illuminate\Http\Resources\Json\JsonResource;

class ContactsCollection extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
    //  return parent::toArray($request);
    return [    
        'first_name' => $this->Name,
        'last_name' => $this->Surname,
        'email' => $this->Email,
        'mobile_number' => $this->Cell
    ];
    }
}
